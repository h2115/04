
import './App.css';
import { Route, Routes } from 'react-router-dom';
import NewPostComponent from './pages/NewPostComponent';
import NavBarComponent from './components/NavBarComponent';
import HomePageComponent from './pages/HomePageComponent';
import UserComponent from './pages/UserComponent';
import UserDetailComponent from './pages/UserDetailComponent';
import UserAccount from './pages/UserAccount';
import UserInfo from './pages/UserInfo';
import UserProfile from './pages/UserProfile';
import { useState } from 'react';
import ProtectedRouteComponent from './pages/ProtectedRouteComponent';
import LoginComponent from './pages/LoginComponent';

function App() {

  return (
    <div className="App">
      <NavBarComponent/>
      <Routes>
        <Route path='/home' element={<HomePageComponent/>}/>
        <Route path='/post' element={<NewPostComponent/>}/>
        <Route path='/category' element={<UserComponent/>}/>
        <Route path='/user/:id' element={<UserDetailComponent/>}/>
      </Routes>
    </div>
  );
}

export default App;
