import axios from "axios";
import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

export default function NewPostComponent() {
  
 
  const [title,setTitle] = useState('')
  const [description,setDescription] = useState('')
  const [isPublished,setIsPublished] = useState(true)
  const [image,setImage] = useState('') 
  const handleOnSubmit = () => {
     axios.post('https://ams.hrd-edu.info/api/articles',{title,description,isPublished})
      .then(res => console.log(res.data.message));
  }

  const handleTitleChange = (e) => {
    setTitle(e.target.value)
    console.log(e.target.value);
  }
  const handleDescriptionChange = (e) => {
    setDescription(e.target.value)
  }
  const handleIsPublishedChange = (e) => {
    setIsPublished(e.target.checked)
    console.log(e.target.checked);
  }
  const handleImageChange = (e) => {
    setImage(e.target.files[0])
    console.log(e.target.files[0]);
  }

  return (
    <div className="container">
      <Form className="w-50">
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control type="text" placeholder="Enter title" onChange={handleTitleChange}/>
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Sescription</Form.Label>
          <Form.Control type="text" placeholder="Description" onChange={handleDescriptionChange}/>
        </Form.Group>
        <Form.Group controlId="formFile" className="mb-3">
    <Form.Label>Default file input example</Form.Label>
    <Form.Control type="file" onChange={handleImageChange}/>
  </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="IsPubished" onChange={handleIsPublishedChange}/>
        </Form.Group>
        <Button variant="primary" onClick={handleOnSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
}
