import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function UserComponent() {
  return (
    <div>
        <Button variant="info" as={Link} to='/user/10'>Click here</Button>
        
    </div>
  )
}
