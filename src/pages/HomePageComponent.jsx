import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import CardComponent from '../components/CardComponent'
import { api } from '../components/service/Api'

export default function HomePageComponent() {

  const [data,setData] = useState([])
  useEffect(() => {
    fetch('https://ams.hrd-edu.info/api/articles')
        .then(res => res.json())
        .then(r => {
          // console.log(r.payload)
          setData(r.payload)
        })
  },[])
  // console.log(payload);
  
  return (
    <div>
      <Container>
        <Row>
            {data.map((item,index) => (
            <Col key={index}>
              <CardComponent item ={item}/>
            </Col>
            ))}    
        </Row>
      </Container> 
    </div>
  )
}
