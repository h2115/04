import React, { useState } from "react";
import { Button, Card } from "react-bootstrap";

export default function CardComponent({ item }) {
  return (
    <div>
      <Card>
        <Card.Img variant="top" src={item.image} />
        <Card.Body>
          <Card.Title>{item.title}</Card.Title>
          <Card.Text>{item.description}</Card.Text>
          <Button variant="success">Go somewhere</Button>
        </Card.Body>
      </Card>
    </div>
  );
}
